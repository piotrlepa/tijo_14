package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class UserIdSpec extends Specification {

    @Unroll
    def "should check correct size for #id"() {
        when:
            def userId = new UserId(id)
        then:
            userId.correctSize == expectedResult
        where:
            id                  | expectedResult
            ''                  | false
            '123'               | false
            '12345678912'       | true
            '1234567891221341'  | false
    }

    @Unroll
    def "should check sex for #id"() {
        when:
            def userId = new UserId(id)
        then:
            userId.sex.orElse(null) == expectedSex
        where:
        id                  | expectedSex
        ''                  | null
        '547'               | null
        '76030146492'       | UserIdChecker.Sex.MAN
        '83101878369'       | UserIdChecker.Sex.WOMAN
    }

    @Unroll
    def "should check correctness for #id"() {
        when:
            def userId = new UserId(id)
        then:
            userId.isCorrect() == expectedResult
        where:
            id                  | expectedResult
            ''                  | false
            '12312'             | false
            '76030146492'       | true
            '83101878369'       | true
            '95101578331'       | false
            'abdtujnopff'       | false
    }

    @Unroll
    def "should check date for #id"() {
        when:
            def userId = new UserId(id)
        then:
            userId.date.orElse(null) == expectedDate
        where:
            id                  | expectedDate
            '76030146492'       | '01-03-1976'
            '83101878369'       | '18-10-1983'
            '06261194353'       | '11-06-2006'
            '95101578331'       | null
            'abdtujnopff'       | null
    }
}
